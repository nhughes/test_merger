from django.db import models
from datetime import datetime, timezone


class Player(models.Model):
    player_url_id = models.CharField('URL ID', max_length=50)
    player_name = models.CharField('Player\'s Name', max_length=50)

    last_update = models.DateTimeField('Last Updated', default=datetime.now())

    def __str__(self):
        return self.player_name


class Season(models.Model):
    player = models.ForeignKey(Player, on_delete=models.CASCADE)

    Unranked = "Unranked"

    season_number = models.IntegerField(default=0)

    solo = models.CharField(
        max_length=20,
        default=Unranked,
    )
    doubles = models.CharField(
        max_length=20,
        default=Unranked,
    )
    solo_standard = models.CharField(
        max_length=20,
        default=Unranked,
    )
    standard = models.CharField(
        max_length=20,
        default=Unranked,
    )
