from django.contrib import admin

from .models import Player


class PlayerAdmin(admin.ModelAdmin):
    list_display = ('player_name', 'player_url_id', 'last_update')

admin.site.register(Player, PlayerAdmin)
