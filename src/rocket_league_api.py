import urllib.request
import re
import database_api
from bs4 import BeautifulSoup
from datetime import datetime, date, timezone

current_season = 7
match_type = [
    'Solo',
    'Doubles',
    'Solo Standard',
    'Standard'
]

# CHange has been made to master

class RLPlayer:
    def __init__(self, player_name, player_id):
        self.player_id = player_id
        self.player_name = player_name
        self.S1_Ranks = []
        self.S2_Ranks = []
        self.S3_Ranks = []
        self.S4_Ranks = []
        self.S5_Ranks = []
        self.S6_Ranks = []
        self.S7_Ranks = []
        self.url = 'http://rltracker.pro/profiles/' + self.player_id + '/steam/'
        self.last_update_time = datetime.now()

        self.get_ranks()

    # get_ranks()
    #
    # Detail:
    #   This function looks up all season ranks of the player and updates the player's data with the new records.
    #
    def get_ranks(self):
        if not database_api.check_player_by_id(self.player_id):
            database_api.create_player(self.player_name, self.player_id)
            self.download_ranks()
            self.update_database_from_player()
            self.last_update_time = database_api.get_player_last_update_time(self.player_name)
        else:
            self.update_player_from_database()
            self.update_if_needed()

    # build_request()
    #
    # Return format:
    #   N/A
    #
    # Detail:
    #   Creates and returns the HTML document of the player's stats page.
    #
    def build_request(self):
        req = urllib.request.Request(
            self.url,
            data=None,
            headers={
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) '
                'Chrome/35.0.1916.47 Safari/537.36'
            }
        )
        return req

    # get_all_ranks()
    #
    # Return format:
    #   {
    #       'S1': [Solo Rank, Doubles Rank, Solo Standard Rank, Standard Rank],
    #       'S2': [Solo Rank, Doubles Rank, Solo Standard Rank, Standard Rank],
    #       'S3': [Solo Rank, Doubles Rank, Solo Standard Rank, Standard Rank],
    #       'S4': [Solo Rank, Doubles Rank, Solo Standard Rank, Standard Rank]
    #   }
    #
    # Detail:
    #   This function returns all season data for a player in the format of a dict.
    #
    def get_all_ranks(self):
        self.update_if_needed()

        ranks = {
            'S1': self.S1_Ranks,
            'S2': self.S2_Ranks,
            'S3': self.S3_Ranks,
            'S4': self.S4_Ranks,
            'S5': self.S5_Ranks,
            'S6': self.S6_Ranks,
            'S7': self.S7_Ranks
        }
        return ranks

    # get_season_rank(season_number)
    #
    # Arguments:
    #   season_number - Expects a number. Season number for season to check. Defaults to current season.
    #       1 - Season 1
    #       2 - Season 2
    #       3 - Season 3
    #       4 - Season 4
    #       5 - Season 5
    #       6 - Season 6
    #       7 - Season 7
    #
    # Return format:
    #   [Solo Rank, Doubles Rank, Solo Standard Rank, Standard Rank]
    #       e.g. ['Unranked', 'Silver II', 'Unranked', 'Silver II']
    #
    # Detail:
    #   This function looks up a player's ranks for a given season and returns the data in a list.
    def get_season_rank(self, season_number=current_season):

        self.update_if_needed()

        if season_number == 1:
            return self.S1_Ranks
        elif season_number == 2:
            return self.S2_Ranks
        elif season_number == 3:
            return self.S3_Ranks
        elif season_number == 4:
            return self.S4_Ranks
        elif season_number == 5:
            return self.S5_Ranks
        elif season_number == 6:
            return self.S6_Ranks
        elif season_number == 7:
            return self.S7_Ranks
        else:
            return "Invalid Season Error"

    # get_current_tier(game_type)
    #
    # Arguments:
    #   game_type - Expects a number.
    #       1 - Solo
    #       2 - Doubles
    #       3 - Solo Standard
    #       4 - Standard
    #
    # Return format:
    #   'Rank'
    #       e.g. 'Gold I'
    #
    # Detail:
    #   This function looks up a player's ranks for a given season and returns the data in a list.
    def get_current_tier(self, game_type):
        self.update_if_needed()

        ranks = self.get_season_rank(current_season)
        return ranks[game_type]

    def update_player_from_database(self):
        self.S1_Ranks = database_api.get_player_season_data(self.player_name, 1)
        self.S2_Ranks = database_api.get_player_season_data(self.player_name, 2)
        self.S3_Ranks = database_api.get_player_season_data(self.player_name, 3)
        self.S4_Ranks = database_api.get_player_season_data(self.player_name, 4)
        self.S5_Ranks = database_api.get_player_season_data(self.player_name, 5)
        self.S6_Ranks = database_api.get_player_season_data(self.player_name, 6)
        self.S7_Ranks = database_api.get_player_season_data(self.player_name, 7)

    def update_database_from_player(self):
        if self.S1_Ranks:
            database_api.set_player_season_data(self.player_name, 1, self.S1_Ranks[0], self.S1_Ranks[1], self.S1_Ranks[2], self.S1_Ranks[3])
        if self.S2_Ranks:
            database_api.set_player_season_data(self.player_name, 2, self.S2_Ranks[0], self.S2_Ranks[1], self.S2_Ranks[2], self.S2_Ranks[3])
        if self.S3_Ranks:
            database_api.set_player_season_data(self.player_name, 3, self.S3_Ranks[0], self.S3_Ranks[1], self.S3_Ranks[2], self.S3_Ranks[3])
        if self.S4_Ranks:
            database_api.set_player_season_data(self.player_name, 4, self.S4_Ranks[0], self.S4_Ranks[1], self.S4_Ranks[2], self.S4_Ranks[3])
        if self.S5_Ranks:
            database_api.set_player_season_data(self.player_name, 5, self.S5_Ranks[0], self.S5_Ranks[1], self.S5_Ranks[2], self.S5_Ranks[3])
        if self.S6_Ranks:
            database_api.set_player_season_data(self.player_name, 6, self.S6_Ranks[0], self.S6_Ranks[1], self.S6_Ranks[2], self.S6_Ranks[3])
        if self.S7_Ranks:
            database_api.set_player_season_data(self.player_name, 7, self.S7_Ranks[0], self.S7_Ranks[1], self.S7_Ranks[2], self.S7_Ranks[3])

    def download_ranks(self):
        tiers = []
        request = self.build_request()
        page = urllib.request.urlopen(request)
        soup = BeautifulSoup(page, "html.parser")
        season_html_tags = soup.find_all('div', "season_div")
        choice = re.compile('.*(season[1-7]_div)')
        season_list = choice.findall(season_html_tags.__str__())
        tier_html_tags = soup.find_all('div', "tier_name")

        for tier in tier_html_tags:
            tiers.append(tier.get_text())

        for season in season_list:
            if season == "season1_div":
                del self.S1_Ranks[:]
                for num in range(0, 4):
                    self.S1_Ranks.append(tiers[num])
                del tiers[:4]

            elif season == "season2_div":
                del self.S2_Ranks[:]
                for num in range(0, 4):
                    self.S2_Ranks.append(tiers[num])
                del tiers[:4]

            elif season == "season3_div":
                del self.S3_Ranks[:]
                for num in range(0, 4):
                    self.S3_Ranks.append(tiers[num])
                del tiers[:4]

            elif season == "season4_div":
                del self.S4_Ranks[:]
                for num in range(0, 4):
                    self.S4_Ranks.append(tiers[num])
                del tiers[:4]

            elif season == "season5_div":
                del self.S5_Ranks[:]
                for num in range(0, 4):
                    self.S5_Ranks.append(tiers[num])
                del tiers[:4]

            elif season == "season6_div":
                del self.S6_Ranks[:]
                for num in range(0, 4):
                    self.S6_Ranks.append(tiers[num])
                del tiers[:4]

            elif season == "season7_div":
                del self.S7_Ranks[:]
                for num in range(0, 4):
                    self.S7_Ranks.append(tiers[num])
                del tiers[:4]

            else:
                print("Error")

    def update_if_needed(self):
        date_now = datetime.now()
        time_difference = date_now - self.last_update_time
        if time_difference.seconds // 3600 > 1:
            self.download_ranks()
            self.update_database_from_player()
            self.last_update_time = database_api.get_player_last_update_time(self.player_name)
# End Class Player


if __name__ == "__main__":
    # Test Function
    print("Hi")
