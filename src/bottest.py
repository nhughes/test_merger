from rocket_league_api import get_player_data


def main():
    response = ""
    player_id = "pmccauley1994"
    player_stats = get_player_data(player_id)

    print(player_stats)

    for k, v in player_stats.items():
        print(k, v)
        if len(v) == 0:
            continue
        else:
            response += k + "\n"
            for i, j in zip(v, ["Solo ", "Doubles ", "Solo Standard ", "Standard "]):
                response += "\t" + j.ljust(14, "-") + "\t" + i + "\n"

        print(response)


if __name__ == "__main__":
    main()
