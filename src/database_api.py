import os
import sys
import django
from datetime import datetime, timezone

current_path = os.getcwd()
path = current_path + '/rl_django'

if path not in sys.path:
    sys.path.append(path)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "rl_django.settings")
django.setup()
from players.models import Player, Season


def get_all_players():
    p_q = Player.objects.all()
    return p_q


def get_player(player_name):
    # TODO player name validation
    p_q = Player.objects.get(player_name=player_name)
    return p_q


def check_player_by_id(player_id):
    # TODO player name validation
    try:
        p_q = Player.objects.get(player_url_id=player_id)
    except Player.DoesNotExist:
        p_q = None
    return False if p_q is None else True


def get_player_id(player_name):
    # TODO player name validation
    p_q = Player.objects.get(player_name=player_name)
    return p_q.player_url_id


def create_player(player_name, player_url_id):
    new_player = Player(player_name=player_name, player_url_id=player_url_id)
    new_player.save()

    season1 = Season(season_number=1)
    season2 = Season(season_number=2)
    season3 = Season(season_number=3)
    season4 = Season(season_number=4)
    season5 = Season(season_number=5)
    season6 = Season(season_number=6)
    season7 = Season(season_number=7)

    season1.player = new_player
    season2.player = new_player
    season3.player = new_player
    season4.player = new_player
    season5.player = new_player
    season6.player = new_player
    season7.player = new_player

    season1.save()
    season2.save()
    season3.save()
    season4.save()
    season5.save()
    season6.save()
    season7.save()

    new_player.season_set.add(season1)
    new_player.season_set.add(season2)
    new_player.season_set.add(season3)
    new_player.season_set.add(season4)
    new_player.season_set.add(season5)
    new_player.season_set.add(season6)
    new_player.season_set.add(season7)

    new_player.save()

    # raise NotImplementedError("create_player is not yet implemented")


def remove_player(player_name):
    player = Player.objects.get(player_name=player_name)
    key = player.id
    Season.objects.get(pk=key).task_set.all().delete()
    player.delete()


def set_player_season_data(player_name, season, solo_rank, doubles_rank, solo_standard_rank, standard_rank):
    player = Player.objects.get(player_name=player_name)

    season_info = player.season_set.all().filter(season_number=season)
    for s in season_info:
        if s.season_number == season:
            s.solo = solo_rank
            s.doubles = doubles_rank
            s.solo_standard = solo_standard_rank
            s.standard = standard_rank
            s.save()
    player.last_update = datetime.now()


def get_player_season_data(player_name, season):
    player = Player.objects.get(player_name=player_name)
    season_info = player.season_set.all().filter(season_number=season)
    return_season = None
    for s in season_info:
        return_season = s

    return [return_season.solo, return_season.doubles, return_season.solo_standard, return_season.standard]


def get_player_last_update_time(player_name):
    player = Player.objects.get(player_name=player_name)

    return player.last_update


def main():
    # create_player("Niall", "77777")

    # players = get_player("Niall")

    # seasons = Season.objects.all().filter(player_id=10)
    # seasons = Season23.objects.all()
    # r.article_set.add(new_article2)

    # for season in seasons:
    #     players.season23_set.add(season)
    #
    # players.save()

    # print(players.season23_set.all())
    #
    # print(players)
    # for season in players.season23_set.all():
    #     print(season)

    print("Hi")


if __name__ == "__main__":
    main()
