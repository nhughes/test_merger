import urllib.request
from bs4 import BeautifulSoup

playerID = 'crypticstorm'

url = 'http://rltracker.pro/profiles/' + playerID + '/steam/'
req = urllib.request.Request(
    url,
    data=None,
    headers={
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
    }
)

page = urllib.request.urlopen(req)
soup = BeautifulSoup(page, "html.parser")
page = soup.find_all('div', "season_selector")
del page[-1]
page = soup.find_all('div', "tier_name")
for thing in page:
    print(thing.get_text())
