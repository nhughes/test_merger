from discord.ext.commands import Bot
from rocket_league_api import get_player_data


my_bot = Bot(command_prefix="!")

# maybe we should change this to database backend and
# create a command to allow us to add/remove/modify players
rl_players = {
    "peter": "pmccauley1994",
    "niall": "76561198046977471",
    "avon": "76561198016977756",
    "sessle": "76561198110467801"
}


@my_bot.event
async def on_read():
    print("Client logged in")


# A command which returns hello world
@my_bot.command()
async def hello(*args):
    return await my_bot.say("Hello, world!")


# a command which echos back what it is sent
@my_bot.command()
async def echo(*args):
    response = " ".join(args)
    return await my_bot.say(response)


# this command return all the stats if the season is not empty
@my_bot.command()
async def stats(*args):
    response = ""
    name = args[0].lower()
    # mode = args[1].lower

    if name in rl_players.keys():
        player_id = rl_players[name]
    else:
        return await my_bot.say("Player %s does not exist" % name)

    player_stats = get_player_data(player_id)

    for k, v in player_stats.items():
        if len(v) == 0:
            continue
        else:
            response += k + "\n"
            for i, j in zip(v, ["Solo ", "Doubles ", "Solo Standard ", "Standard "]):
                response += "\t" + j.ljust(15, "-") + "- " + i + "\n"

    return await my_bot.say(response)


# returns everyones doubles rank
# need to do solo, standard and solo standard as well
# we should create on base method to do it all and then the
# commands can be one ot two lines that pass data to it (peter will do this)
@my_bot.command()
async def doubles(*args):
    response = ""
    player_ranks = dict({})
    for name, player_id in rl_players.items():
        player_stats = get_player_data(player_id)
        season_data = player_stats["S4"]
        if len(season_data) == 0:
            continue
        else:
            player_ranks[name] = season_data[1]

    for name, rank in player_ranks.items():
        response += name + "\t" + rank + "\n"

    if response == "":
        return await my_bot.say("No player data found for doubles")
    return await my_bot.say(response)


# this is to list all players currently registered
@my_bot.command()
async def players(*args):
    response = "List of players available\n\t"
    names = [name for name, player_id in rl_players.items()]

    response += ", ".join(names)

    return await my_bot.say(response)


# server log, we should implement proper logging
print("Bot started")
my_bot.run("Mjg4Nzc3MjM5Mjc5OTYwMDY0.C-kyNg.QOBPel7kTQDjG3GlXDTK1pCeZw8")  # never tell this to anyone
